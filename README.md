# MySQL Training at Heitech

Date : 31 Oct - 04 Nov 2022

Location : Heitech Village, Subang

## Introduction :
This course will use the concept of "From Zero to Hero", meaning that we will teach you from basic to mastering MySQL. In order to fully grasp the content of the class, students are expected to have a basic database concept and SQL. Student without basic concept of datbase are welcome to join the class but must expect not to fully grasp the whole content.


## Tools that will be used in this class are as follows :-

MySQL 8
Linux (Centos)
Windows 10 / 11
MySQL Workbench
Terminal / Command Line
Each student should have the following minimum PC / Notebook specification recommendation:

Window 10 with 8 GB RAM
45 GB Free Storage
